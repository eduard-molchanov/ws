
## Задача 4

---
1. Запустить Docker. 
2. ![screenshot of sample](img/start-docker.jpg)

3. Склонировать  [репозиторий](https://bitbucket.org/eduard-molchanov/ws/src/master/) .

4. При нажатом **Shift**  правой мышей нажать на каталог с репозиторием и выбрать указанные на скрине пункт

5. ![screenshot of sample](img/bash.jpg)

6. в открывшейся консоле выполнить следующую команду:
   **docker build -t task-4 .**  
7. ![screenshot of sample](img/command-1.jpg)

8. После сборки 

9. ![screenshot of sample](img/assembly.jpg)

10. выполнить запуск: **docker run task-4**

11. ![screenshot of sample](img/run.jpg)

12. должен получиться следующий резутьтат

13. ![screenshot of sample](img/result.jpg)